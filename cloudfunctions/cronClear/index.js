// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext()
	let date = new Date()
	date.setDate(date.getDate()-7)
	return await db.collection('crontab').where({
		create_time:_.lt(date)
	}).remove()
}