// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  if (event.status!=null){
  return  db.collection("shop").where({
    data:{
      status: event.status
    }
  }).skip(event.skip).
  limit(event.limit).get()
  }else{
    return db.collection("shop").skip(event.skip).
      limit(event.limit).get()
  }
  
}