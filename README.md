# 物资预约领取小程序

疫情之下，提倡减少出门、佩戴口罩。而目前阶段口罩等物资在线下领取时存在排队时间长、人群集中的情况，反而适得其反。为了减少线下流程，本小程序以分时间段、分地点的方式可以实现对物资领取人群的分流，同时可以设置自提和送货上门两种方式，随机抽签和先到先得两种预约模式。门店可开启关联其他门店的功能，以实现一定地区一定时间一定次数内预约重复校验。用户预约成功后，可凭借预约信息二维码与身份证号至指定门店领取物资，门店员工可扫描二维码以查看预约信息。

## 特性

1. 预约分时、分点实现分流
2. 校验预约间隔与次数，如：三天内仅可预约一次
3. 支持多门店在同一规则下预约，可应用于同一城市下统一管理，如：门店A与门店B的预约规则都为三天内仅可预约一次，P在门店A预约成功了，那么他三天内均无法在AB店预约
4. 支持预约抽签和先到先得两种模式
5.支持送货上门与门店自提两种领取模式

## 依赖

- 云开发
- ColorUI

## 部署说明

1. 请确保您的小程序已开通云开发并且有医疗执照
2. 下载源码
3. 上传云函数文件夹中(cloudfunctions)的每个云函数，其中cron开头的云函数为定时触发器，还需要上传触发器，admin开头的云函数为管理操作
4. 上传小程序代码
5. 提交审核

[Wiki](https://gitee.com/crystar/tcb-hackthon-mask/wikis/%E7%89%A9%E8%B5%84%E9%A2%84%E7%BA%A6%E9%A2%86%E5%8F%96%E4%BA%91%E5%B9%B3%E5%8F%B0 "Wiki")

## 开发说明

未完待续

## Bug 反馈

如果有 Bug ，请通过 [Issues](https://gitee.com/crystar/tcb-hackthon-mask/issues/new "Issues") 反馈

## 联系方式

VX：code_badminton

## LICENSE

[MIT](https://gitee.com/crystar/tcb-hackthon-mask/blob/master/LICENSE "MIT")

## 界面预览

![首页](https://images.gitee.com/uploads/images/2020/0210/224031_4f975d97_405207.jpeg "1.jpg")
![门店详情](https://images.gitee.com/uploads/images/2020/0210/224044_bb780037_405207.jpeg "2.jpg")
![预约](https://images.gitee.com/uploads/images/2020/0210/224103_c287edb2_405207.jpeg "3.jpg")
![用于预约记录](https://images.gitee.com/uploads/images/2020/0210/224453_c6b23de4_405207.jpeg "5.jpg")
![门店入驻](https://images.gitee.com/uploads/images/2020/0210/224551_f7e0b569_405207.jpeg "6.jpg")
![预约设置](https://images.gitee.com/uploads/images/2020/0210/224719_e195777c_405207.jpeg "9.jpg")
![管理预约记录](https://images.gitee.com/uploads/images/2020/0210/224834_02812dcf_405207.jpeg "10.jpg")
![管理预约详情](https://images.gitee.com/uploads/images/2020/0210/224845_c1ba1e84_405207.jpeg "11.jpg")
![员工管理](https://images.gitee.com/uploads/images/2020/0210/224855_8c6b9fcf_405207.jpeg "员工管理.jpg")
