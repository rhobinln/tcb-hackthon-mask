const app = getApp()
Page({
	data: {
		products: [],
		product: 0,

		type: 1,
		timelines: [],
		time_begin: '',
		time_end: '',
		time_result: '',
		is_allow: false,

		modalName: '',
		modalTimeline: {
			time_begin: '',
			time_end: '',
			nums_max: '',
			nums_limit: true
		},
		modalIndex: ''
	},

	onLoad: function (options) {
		app.db.collection('product').where({
			shop_id: options.shop_id
		}).orderBy('create_time', 'asc').field({
			_id: true,
			name: true
		}).get().then(res => {
			//生成今天日期
			let date = new Date()
			let year = date.getFullYear(),
				month = date.getMonth() + 1,
				day = {
					year: date.getFullYear(),
					month: date.getMonth() + 1,
					day: date.getDate()
				}
			this.setData({
				products: res.data,
				year,
				month,
				day,
				shop_id: options.shop_id
			})
			this.makeCalendar()
		})
	},
	changeallow(e) {
		this.setData({
			is_allow: e.detail.value
		})
	},
	copy(e) {
		wx.showModal({
			  title:'操作确认',
			  content:'复制设置的同时将会清空今天的数据'
		})
		app.cloud({
			name: 'shopAppointSettingCopy',
			data:{date:this.data.day,from:e.detail.value},
			loading: '提交中',
			def: true,
			success: res => {
				wx.showToast({
					title: res.msg,
				})
			}
		})
	},
	onSubmit(e) {
		let data = e.detail.value
		data.shop_id = this.data.shop_id
		data.product_id = this.data.products[this.data.product]._id
		data.day = this.data.day.day
		data.year = this.data.day.year
		data.month = this.data.day.month
		data.time_begin = this.data.time_begin
		data.time_end = this.data.time_end
		data.time_result = this.data.time_result
		data.type = this.data.type
		data.timelines = this.data.timelines
		//校验时间顺序
		let time_begin = new Date(data.time_begin),
			time_end = new Date(data.time_end)
		if (time_begin.getTime() >= time_end.getTime()) {
			return wx.showToast({
				title: '开放预约结束时间必须晚于开始预约结束时间',
				icon: 'none'
			})
		}
		if (data.type == 1) {
			let time_result = new Date(data.time_result)
			if (time_result.getTime() < time_end.getTime()) {
				return wx.showToast({
					title: '抽签时间不得早于开放预约结束时间',
					icon: 'none'
				})
			}
		}
		app.cloud({
			name: 'shopAppointSetting',
			data,
			loading: '提交中',
			def: true,
			success: res => {
				wx.showToast({
					title: res.msg,
				})
				this.getData()
			}
		})
	},
	dayChoose(e) {
		this.setData({
			day: {
				year: e.detail.year,
				month: e.detail.month,
				day: e.detail.day,
			}
		})
		this.makeCalendar()
	},
	makeCalendar() {
		let days_style = []
		if (this.data.year == this.data.day.year && this.data.month == this.data.day.month) {
			days_style.push({
				month: 'current',
				day: this.data.day.day,
				color: '#FFFFFF',
				background: '#59518d' //59518d
			})
		}
		this.setData({
			days_style
		})
		this.getData()
	},
	changeMonth(e) {
		this.setData({
			year: e.detail.currentYear,
			month: e.detail.currentMonth
		})
		this.makeCalendar()
	},
	tabSelect(e) {
		this.setData({
			product: e.currentTarget.dataset.ind
		})
		this.getData()
	},

	handleChange(e) {
		this.setData({
			[e.currentTarget.dataset.key]: e.detail.dateString
		})
	},

	changeValue(e) {
		this.setData({
			[e.currentTarget.dataset.key]: e.detail.value
		})
	},

	getData() {
		wx.showLoading({
			title: '加载中',
			mask: true
		})
		//查询今日设置
		app.db.collection('daily').where({
			day: this.data.day.day,
			month: this.data.day.month,
			year: this.data.day.year,
			shop_id: this.data.shop_id,
			product_id: this.data.products[this.data.product]._id
		}).field({
			type: true,
			time_begin: true,
			time_end: true,
			time_result: true,
			is_allow: true,
			_id: true
		}).get().then(res => {
			wx.hideLoading()
			if (res.data.length > 0) {
				let data = res.data[0]
				console.log(data)
				app.db.collection('timeline').where({
					daily_id: data._id
				}).field({
					shop_id: false,
					daily_id: false
				}).orderBy('time_begin','asc').orderBy('time_end','asc').get().then(res => {
					console.log(res)
					this.setData({
						type: data.type,
						timelines: res.data,
						time_begin: app.getDatetime(data.time_begin),
						time_end: app.getDatetime(data.time_end),
						time_result: data.time_result ? app.getDatetime(data.time_result) : '',
						is_allow: data.is_allow
					})
				})

			} else {
				let date = new Date(this.data.day.year, this.data.day.month - 1, this.data.day.day)
				date.setDate(date.getDate() - 1)
				date = app.getDate(date)
				let data = {
					type: 1,
					timelines: [],
					time_begin: date + ' 09:00:00',
					time_end: date + ' 21:00:00',
					time_result: date + ' 22:00:00',
					is_allow: false
				}
				this.setData(data)
			}
		})
	},
	hideModal() {
		this.setData({
			modalName: ''
		})
	},
	changeTimeline(e) {
		this.setData({
			['modalTimeline.' + e.currentTarget.dataset.key]: e.detail.value
		})
	},
	changeLimit(e) {
		this.setData({
			['modalTimeline.nums_limit']: e.detail.value
		})
	},
	timeline(e) {
		console.log(e)
		if (e.currentTarget.dataset.ind == "add") {
			this.setData({
				modalName: 'timeline',
				modalTimeline: {
					time_begin: '',
					time_end: '',
					nums_max: '',
					nums_limit: true
				},
				modalIndex: 'add'
			})
		} else {
			this.setData({
				modalName: 'timeline',
				modalTimeline: this.data.timelines[e.currentTarget.dataset.ind],
				modalIndex: e.currentTarget.dataset.ind
			})
		}

	},
	edittimeline(e) {
		let data = e.detail.value
		data.nums_limit = this.data.modalTimeline.nums_limit
		data.time_begin = this.data.modalTimeline.time_begin
		data.time_end = this.data.modalTimeline.time_end
		if (this.data.modalTimeline._id) {
			data._id = this.data.modalTimeline._id
		}
		if (!data.time_begin) {
			return wx.showToast({
				title: '请选择开始时间',
				icon: 'none'
			})
		}
		if (!data.time_end) {
			return wx.showToast({
				title: '请选择结束时间',
				icon: 'none'
			})
		}
		if (this.data.type == 1 || data.nums_limit) {
			if (parseInt(data.nums_max) < 0 || data.nums_max.length == 0) {
				return wx.showToast({
					title: '请输入可领取次数',
					icon: 'none'
				})
			}
		}

		//todo校验时间
		let timelines = this.data.timelines
		if (this.data.modalIndex == 'add') {
			timelines.push(data)
		} else {
			timelines[this.data.modalIndex] = data
		}
		this.setData({
			timelines,
			modalName: ''
		})
	},
	deleteTimeline(e) {
		wx.showModal({
			title: '操作提示',
			content: '您确认要删除这个时间段吗？',
			success: res => {
				if (res.confirm) {
					let timelines = this.data.timelines
					if (!timelines[e.currentTarget.dataset.ind]._id) {
						timelines.splice(e.currentTarget.dataset.ind, 1)
					} else {
						timelines[e.currentTarget.dataset.ind].delete = true
					}
					this.setData({
						timelines
					})
				}

			}
		})

	}

})