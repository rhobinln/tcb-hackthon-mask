const app = getApp()
Page({

	data: {
		statusAppointTitle: [],
		status: 0,
		result: '加载中',
		statusAppointTitles: app.globalData.statusAppointTitle,
		list: [],
		date: '',
		search: ''
	},
	tabSelect(e) {
		this.setData({
			status: e.currentTarget.dataset.status,
			list: []
		})
		this.getData()
	},
	onLoad: function (options) {
		let statusAppointTitle = [{
			text: '全部',
			value: '0'
		}]
		for (let x in app.globalData.statusAppointTitle) {
			statusAppointTitle.push({
				text: app.globalData.statusAppointTitle[x].text,
				value: x
			})
		}
		this.setData({
			statusAppointTitle
		})
		this.setData(options)
		this.getData()
	},

	getData() {
		app.cloud({
			name: 'shopAppoints',
			data: {
				shop_id: this.data.shop_id,
				skip: this.data.list.length,
				status: this.data.status,
				date: this.data.date,
				search: this.data.search
			},
			loading: '加载中',
			success: res => {
				wx.stopPullDownRefresh()
				console.log(res)
				let list = this.data.list.concat(res.data)
				this.setData({
					result: '',
					list
				})
			},
			fail: res => {
				wx.stopPullDownRefresh()
				this.setData({
					result: res.msg
				})
			}

		})
		return
	},
	onPullDownRefresh() {
		this.setData({
			list: []
		})
		this.getData()
	},
	onReachBottom() {
		this.getData()
	},
	chooseDate(e) {
		this.setData({
			date: e.detail.value,
			list: []
		})
		this.getData()
	},
	scan() {
		app.scancode()
	},
	search(e) {
		this.setData({
			search: e.detail.value,
			list: []
		})
		this.getData()
	}
})