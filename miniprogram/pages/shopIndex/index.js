const app = getApp()
const base64 = require('../../utils/base64.js')

Page({

	data: {
		elements: [{
				title: '门店设置',
				name: '门店基础信息',
				color: 'purple',
				icon: 'home',
				url: 'Setting',
				rule: 'setting'
			},
			{
				title: '校验规则',
				name: '用于重复预约规则校验',
				color: 'green',
				url: 'Codes',
				icon: 'barcode',
				rule: 'codes'
			},
			{
				title: '项目管理 ',
				name: '可预约项目',
				color: 'mauve',
				icon: 'list',
				url: 'Products',
				rule: 'product'
			},
			{
				title: '扫一扫',
				name: '添加员工或查看预约记录',
				color: 'orange',
				icon: 'scan',
				url: 'scan',
				rule: 'scan'
			},
			
			{
				title: '预约设置',
				name: '设置每日可预约项目的时间、数量',
				color: 'pink',
				icon: 'formfill',
				url: 'AppointSetting',
				rule: 'appointsetting'
			},
			{
				title: '预约记录',
				name: '查阅与管理预约记录',
				color: 'brown',
				icon: 'newsfill',
				url: 'Appoints',
				rule: 'appoints'
			},
			{
				title: '员工管理',
				name: '管理门店的员工',
				color: 'red',
				icon: 'group',
				url: 'Staffs',
				rule: 'staff'
			},
			
			{
				title: '访问门店',
				name: '您可分享门店页面至微信会话窗口',
				color: 'grey',
				icon: 'hop',
				url: 'index'
			},
			{
				title: '帮助说明',
				name: '小程序使用说明',
				color: 'olive',
				url: 'help',
				icon: 'text'
			},
			/**{
				title: '物料下载',
				name: '下载可打印的资料',
				color: 'cyan',
				url: 'Download',
				icon: 'pulldown'
			},**/
		],
		result: '页面加载中'
	},
	onLoad: function (options) {
		//校验shops管理员权限
		this.setData(options)
		app.cloud({
			name: 'shopIndex',
			data: {
				_id: this.data._id
			},
			success: res => {
				this.setData({
					result: ''
				})
				this.setData(res.data)
			},
			fail: res => {
				this.setData({
					result: res.msg
				})
			}
		})
	},
	ele(e) {
		let item = this.data.elements[e.currentTarget.dataset.ind],
			that = this
		if (item.url == 'scan') {
			//扫一扫

			app.scancode(this.data._id);
		} else if (item.url == 'index') {
			wx.navigateTo({
				url: '/pages/shop/index?scene=' + this.data._id,
			})
		} else if (item.url == 'help') {
			wx.navigateTo({
				url: '/pages/helps/index',
			})
		} else {
			wx.navigateTo({
				url: '/pages/shop' + item.url + '/index?shop_id=' + this.data._id,
			})
		}
	},
})