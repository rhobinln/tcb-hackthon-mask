const app = getApp()
Page({
	data: {
		list:[
		]
	},
	onLoad(options){
		this.setData(options)
		this.getData()
	},
	getData(){
		wx.showLoading({
			title:'加载中',
			mask:true
		})
		app.db.collection('code').where({
			shop_id:this.data.shop_id
		}).skip(this.data.list.length).limit(15).get().then(res=>{
			wx.stopPullDownRefresh()
			wx.hideLoading()
			if(res.data.length==0){
				this.setData({
					prompt:'还没有创建过校验规则'
				})
				return 
			}
			let list = this.data.list
			list = list.concat(res.data)
			this.setData({list})
		})
	},
	onPullDownRefresh(){
		this.setData({list:[]})
		this.getData()
	},
	onReachBottom(){
		this.getData()
	}

})