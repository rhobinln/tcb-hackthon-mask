Component({
	properties: {
		value: {
			type: Number,
			value: 1
		}
	},
	options: {
		addGlobalClass: true,
	},

	lifetimes: {
		attached: function () {
			this.setData({
				minusStatus: 'disabled'
			});
		},
	},
	methods: {
		check() {
			if (this.data.value <= 1) {
				this.setData({
					minusStatus: 'disabled'
				});
			} else {
				this.setData({
					minusStatus: 'normal'
				});
			}
		},
		bindMinus: function () {
			var num = this.data.value;
			if (num > 1) {
				num--;
			}
			this.setData({
				value: num,
			});
			this.check();
			this.triggerEvent('change', {
				value:num
			})
		},
		bindPlus: function () {
			var num = this.data.value;
			num++;
			this.setData({
				value: num,
			});
			this.check();
			this.triggerEvent('change', {
				value:num
			})
		},
		bindManual: function (e) {
			var num = e.detail.value;
			this.setData({
				value: num
			});
			this.check();
			this.triggerEvent('change', {
				value:num
			})
		}
	}
})